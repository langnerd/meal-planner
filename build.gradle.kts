plugins {
    application
    kotlin("jvm") version "1.3.61"
    id("com.github.johnrengelman.shadow") version "5.0.0"
}

group = "com.justai.jaicf"
version = "1.0.0"

val jaicf = "0.8.2"
val slf4j = "1.7.30"
val ktor = "1.3.1"
val ktorm = "3.2.0"
val mysql = "5.1.6"
val poi = "4.1.2"

application {
    mainClassName = "com.justai.jaicf.template.channel.TelegramKt"
}

repositories {
    mavenLocal()
    mavenCentral()
    jcenter()
    maven(uri("https://jitpack.io"))
}

dependencies {
    implementation(kotlin("stdlib-jdk8"))

    implementation("org.slf4j:slf4j-simple:$slf4j")
    implementation("org.slf4j:slf4j-log4j12:$slf4j")

    implementation("com.justai.jaicf:core:$jaicf")
    implementation("com.justai.jaicf:rasa:$jaicf")
    implementation("com.justai.jaicf:telegram:$jaicf")
    implementation("com.justai.jaicf:mongo:$jaicf")
    implementation("com.justai.jaicf:facebook:$jaicf")

    implementation("io.ktor:ktor-server-netty:$ktor")
    implementation("io.ktor:ktor-client-jackson:$ktor")
    compile("ch.qos.logback:logback-classic:1.2.3")
    compile("org.ktorm:ktorm-core:$ktorm")
    compile ("mysql:mysql-connector-java:$mysql")
    compile("org.apache.poi:poi:$poi")
    compile("org.apache.poi:poi-ooxml:$poi")
}

tasks {
    compileKotlin {
        kotlinOptions.jvmTarget = "1.8"
    }
    compileTestKotlin {
        kotlinOptions.jvmTarget = "1.8"
    }
}

tasks.withType<Jar> {
    manifest {
        attributes(
            mapOf(
                "Main-Class" to application.mainClassName
            )
        )
    }
}

tasks.create("stage") {
    dependsOn("shadowJar")
}
