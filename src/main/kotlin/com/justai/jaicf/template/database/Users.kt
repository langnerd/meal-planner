package com.justai.jaicf.template.database

import com.justai.jaicf.template.entities.User
import org.ktorm.schema.*

object Users: Table<User>("users") {
    val id = int("id").primaryKey().bindTo { it.id }
    val username = varchar("name").bindTo { it.username }
}