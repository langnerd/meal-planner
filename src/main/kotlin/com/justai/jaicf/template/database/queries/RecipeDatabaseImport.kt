package com.justai.jaicf.template.database.queries

import com.justai.jaicf.template.database.Recipes
import org.apache.poi.ss.usermodel.WorkbookFactory
import org.apache.poi.xssf.usermodel.XSSFWorkbook
import org.ktorm.database.Database
import org.ktorm.dsl.insert
import java.io.FileInputStream

class RecipeDatabaseImport(val db: Database) {
    private val nutPattern: Regex = Regex("peanut|nuts|cashew|almond")
    private val nonVeganPattern: Regex = Regex("milk|cheese|egg|butter|mozzarella")
    private val nonVegePattern: Regex = Regex("pork|beef|chicken|lamb|steak|bacon|ham|meat")
    private val themeList1 : Collection<String> = setOf("halloween","christmas","world cuisine")
    private val themeList2 : Collection<String> = setOf("summer", "popular", "local produce")
    private val mealList : Collection<String> = setOf("breakfast", "lunch", "dinner", "dessert", "snack")
    private val dessertPattern: Regex = Regex("Cookies|Cakes|Pies")
    private val lunchPattern: Regex = Regex("Pies|Salads|Soup")
    private val snackPattern: Regex = Regex("Pies|Salads")

    fun readFromExcelFile(filepath: String): RecipeDatabaseImport {
        val inputStream = FileInputStream(filepath)
        //Instantiate Excel workbook using existing file:
        var xlWb = WorkbookFactory.create(inputStream)

        //Get reference to sheet:
        val recipeSheet = xlWb.getSheetAt(0)
        // tags, themes, meals and allergyGroups for now are populated randomly
        for (row in 1..228) {
            var ingredients = mutableListOf<String>()
            var allergyGroups = mutableListOf<String>()
            var tags = mutableListOf<String>("vegetarian", "vegan", "sugar-free")
            var themes = mutableListOf<String>(themeList1.random(), themeList2.random())

            // Ingredients are gathered from multiple cells
            for (col in 4..59 step 3) {
                var ingredient = recipeSheet.getRow(row).getCell(col)?.stringCellValue
                if (ingredient != null) {
                    ingredients.add(ingredient)

                    if (ingredient.contains(nutPattern) && !allergyGroups.contains("nuts")) allergyGroups.add("nuts")
                    if (ingredient.contains(nonVeganPattern)) tags.remove("vegan")
                    if (ingredient.contains(nonVegePattern)) {
                        tags.remove("vegetarian")
                        tags.remove("vegan")
                    }
                    if (ingredient.contains("sugar")) tags.remove("sugar-free")
                }
            }
            // Cells need to be checked for NULL
            var instructions = recipeSheet.getRow(row).getCell(1)?.stringCellValue ?: ""
            var category = recipeSheet.getRow(row).getCell(59)?.stringCellValue ?: ""
            var meals = mutableSetOf<String>()
            if (category.contains(dessertPattern)) meals.add("dessert")
            if (category.contains(lunchPattern)) meals.add("lunch")
            if (category.contains(snackPattern)) meals.add("snack")

            db.insert(Recipes) {
                set(it.recipeName, recipeSheet.getRow(row).getCell(0).stringCellValue)
                set(it.ingredients, ingredients.joinToString(":"))
                set(it.instructions, instructions)
                set(it.allergyGroups, allergyGroups.joinToString())
                set(it.tags, tags.joinToString())
                set(it.category, category)
                set(it.meal, meals.joinToString())
                set(it.theme, themes.joinToString())
            }
        }
        return this
    }
}