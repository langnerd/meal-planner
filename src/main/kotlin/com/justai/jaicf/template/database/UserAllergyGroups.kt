package com.justai.jaicf.template.database

import com.justai.jaicf.template.entities.UserAllergyGroup
import org.ktorm.schema.Table
import org.ktorm.schema.*

object UserAllergyGroups : Table<UserAllergyGroup>("user_allergy_groups") {
    val userId = int("user_id").bindTo { it.user.id }
    val allergyName = varchar("allergy_name").bindTo { it.allergy.allergyName }
}