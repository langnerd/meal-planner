package com.justai.jaicf.template.database

import com.justai.jaicf.template.entities.UserPreference
import org.ktorm.schema.*

object UserPreferences: Table<UserPreference>("user_preferences")  {
    val id = int("id").primaryKey().bindTo { it.id }
    val userId = int("user_id").bindTo { it.user.id }
    val product = varchar("product").bindTo { it.product }
    val preferenceType = int("preference_type").bindTo { it.preferenceType } // 1 - positive, 0 - neutral, -1 - negative
    val preferenceReason = varchar("preference_reason").bindTo { it.preferenceReason } // allergy, dislike etc.
}