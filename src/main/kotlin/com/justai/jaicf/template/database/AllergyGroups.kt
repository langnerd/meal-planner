package com.justai.jaicf.template.database

import com.justai.jaicf.template.entities.AllergyGroup
import org.ktorm.schema.*

object AllergyGroups: Table<AllergyGroup>("allergy_groups") {
    val allergyName = varchar("allergy_name").bindTo { it.allergyName }
    val product = varchar("product").bindTo { it.product } // string separated by commas
}