package com.justai.jaicf.template.database

import com.justai.jaicf.template.entities.Recipe
import org.ktorm.schema.*

object Recipes: Table<Recipe>("recipes") {
    val id = int("id").primaryKey().bindTo { it.id }
    val recipeName = varchar("recipe_name").bindTo { it.recipeName }
    val ingredients = varchar("ingredients").bindTo { it.ingredients }
    val instructions = varchar("instructions").bindTo { it.instructions }
    val allergyGroups = varchar("allergy_groups").bindTo { it.allergyGroups }
    val tags = varchar("tags").bindTo { it.tags } // string separated by commas. Tags relate to food suitability
    val meal = varchar("meal").bindTo { it.meal } // string separated by commas. Meal can be breakfast, lunch etc.
    val category = varchar("category").bindTo { it.category } // string separated by commas. Meal can be breakfast, lunch etc.
    val theme = varchar("theme").bindTo { it.theme } // string separated by commas. Themes: halloween, xmas, summer, winter etc.
}