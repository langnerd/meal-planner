package com.justai.jaicf.template.scenario

import com.github.messenger4j.send.MessagePayload
import com.github.messenger4j.send.MessagingType
import com.github.messenger4j.send.message.Message
import com.github.messenger4j.send.message.TextMessage
import com.github.messenger4j.send.message.quickreply.TextQuickReply
import com.justai.jaicf.channel.facebook.api.facebook
import com.justai.jaicf.channel.facebook.facebook
import com.justai.jaicf.context.ActionContext
import com.justai.jaicf.model.scenario.Scenario
import com.justai.jaicf.template.service.RecipeServiceFactory
import java.util.*

object MainScenario: Scenario(
    dependencies = listOf(CookScenario)
) {
    private val recipeService = RecipeServiceFactory.make()

    init {
        state("main") {
            activators {
                intent("greet")
                regex("/start")
            }

            action {
                // Fetch user's profile
                val user = reactions.facebook?.queryUserProfile()
                val message = "Hi, ${user?.firstName()}! Let me help you cook and plan your meals."
                this.quickReplies(message, Pair("Help me cook", "cook"), Pair("Plan my meals", "plan"))
            }
        }
        state("plan") {
            activators {
                intent("plan")
            }

            action {
                this.sendPayload(TextMessage.create("got you, meal plan it is"))
            }
        }
        state("bye") {
            activators {
                intent("goodbye")
            }

            action {
                reactions.sayRandom("Bye bye!", "See you latter!")
            }
        }

        state("smalltalk") {
            activators {
                intent("bot_challenge")
            }

            action {
                reactions.sayRandom("Yep! I'm a bot.", "Yes, I am.")
            }
        }

        fallback {
            reactions.run {
                sayRandom("Sorry, I didn't get that...", "Looks like it's something new for me...")
                say("Could you repeat please?")
            }
        }
    }
}

fun ActionContext.sendPayload(vararg messages: Message): Unit {
    val fbRequest = this.request.facebook
    messages.map {
        MessagePayload.create(fbRequest?.event?.senderId()!!, MessagingType.RESPONSE, it)
    }.forEach { this.reactions.facebook?.send(it) }
}

fun ActionContext.quickReplies(text: String, vararg replyPairs: Pair<String, String>): Unit {
    val quickReplies = if (replyPairs.isEmpty()) emptyList<TextQuickReply>() else replyPairs.map { TextQuickReply.create(it.first, it.second) }
    this.sendPayload(TextMessage.create(text, Optional.of(quickReplies), Optional.empty()))
}
