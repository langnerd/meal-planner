package com.justai.jaicf.template.scenario

import com.justai.jaicf.model.scenario.Scenario

object BreakfastScenario : Scenario() {
    init {
        state("breakfast") {
            activators {
                regex("Breakfast")
            }
            action {
                this.quickReplies( "Choose how to start your day :)",
                    Pair("❤️ Hearty meal", "hearty_breakfast"),
                    Pair("\uD83C\uDF73 English style", "english_breakfast"),
                    Pair("\uD83C\uDF4B Vitamin bomb", "fruity_breakfast")
                )
            }
        }
    }
}