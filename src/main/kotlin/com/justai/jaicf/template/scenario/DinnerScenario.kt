package com.justai.jaicf.template.scenario

import com.justai.jaicf.model.scenario.Scenario

object DinnerScenario : Scenario() {
    init {
        state("dinner") {
            activators {
                regex("Dinner")
            }
            action {
                this.quickReplies( "Ah, the romance of the sunset .. \uD83E\uDD42",
                    Pair("\uD83D\uDE34 Sweet dreams menu", "light_dinner"),
                    Pair("\uD83D\uDE29 Long day sorrows", "heavy_dinner"),
                    Pair("\uD83D\uDE44 Just eat!", "standard_dinner")
                )
            }
        }
    }
}