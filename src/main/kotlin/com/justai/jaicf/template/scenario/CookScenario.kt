package com.justai.jaicf.template.scenario

import com.github.messenger4j.send.message.TemplateMessage
import com.github.messenger4j.send.message.TextMessage
import com.github.messenger4j.send.message.template.ButtonTemplate
import com.github.messenger4j.send.message.template.button.PostbackButton
import com.justai.jaicf.channel.facebook.api.FacebookQuickReplyBotRequest
import com.justai.jaicf.model.scenario.Scenario

object CookScenario : Scenario(
    dependencies = listOf(BreakfastScenario, LunchScenario, DinnerScenario, RecipeScenario, ShoppingCartScenario)
) {
    init {
        state("cook") {
            activators {
                regex("Help me cook")
            }
            action {
                val event = (request as FacebookQuickReplyBotRequest).event
                when (event.payload()) {
                    "cook" -> {
                        this.quickReplies("Great! What meal are we looking at?",
                            Pair("Breakfast", "breakfast"),
                            Pair("Lunch", "lunch"),
                            Pair("Dinner", "dinner")
                        )
                    }
                    "plan" -> {
                        this.sendPayload(
                            TemplateMessage.create(
                                ButtonTemplate.create(
                                    "Ok, tell me a little bit about yourself. Do you have food allergies?", listOf(
                                        PostbackButton.create("I have food allergies", "allergies")
                                    )
                                )
                            )
                        )
                        this.sendPayload(TextMessage.create("got you, meal plan it is"))
                    }
                    else -> this.sendPayload(TextMessage.create("sorry, I don't handle this action just yet"))
                }
            }
        }
    }
}