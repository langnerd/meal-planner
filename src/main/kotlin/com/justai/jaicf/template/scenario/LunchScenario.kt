package com.justai.jaicf.template.scenario

import com.justai.jaicf.model.scenario.Scenario

object LunchScenario : Scenario() {
    init {
        state("lunch") {
            activators {
                regex("Lunch")
            }
            action {
                this.quickReplies( "What's your midday feast? :)",
                    Pair("\uD83E\uDD69 Meaty pleasure", "meaty_lunch"),
                    Pair("\uD83E\uDD6C Veggie indulgence", "veggie_lunch"),
                    Pair("\uD83E\uDDBE Feeling brave", "spicy_lunch")
                )
            }
        }
    }
}