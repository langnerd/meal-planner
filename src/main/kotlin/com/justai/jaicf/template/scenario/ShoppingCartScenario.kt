package com.justai.jaicf.template.scenario

import com.github.messenger4j.send.message.TemplateMessage
import com.github.messenger4j.send.message.TextMessage
import com.github.messenger4j.send.message.template.ButtonTemplate
import com.github.messenger4j.send.message.template.ReceiptTemplate
import com.github.messenger4j.send.message.template.button.PostbackButton
import com.github.messenger4j.send.message.template.receipt.Address
import com.github.messenger4j.send.message.template.receipt.Adjustment
import com.github.messenger4j.send.message.template.receipt.Item
import com.github.messenger4j.send.message.template.receipt.Summary
import com.justai.jaicf.channel.facebook.api.FacebookPostBackBotRequest
import com.justai.jaicf.channel.facebook.facebook
import com.justai.jaicf.context.ActionContext
import com.justai.jaicf.model.scenario.Scenario
import com.justai.jaicf.template.service.Ingredient
import java.net.URL
import java.time.Instant
import java.util.*

object ShoppingCartScenario : Scenario() {
    init {
        state("shopping_cart") {
            activators {
                event("post_back")
            }
            action {
                val event = (request as FacebookPostBackBotRequest).event
                val chunks = event.payload().filter { it.startsWith("shop_") }.map { it.split("_") }.orElse(emptyList())

                if (chunks.size < 2) {
                    this.sendPayload(TextMessage.create("Oops, there are no ingredients to buy! :("))
                } else {
                    this.sendPayload(TextMessage.create("Shopping ingredients for ${chunks[1]}"))

                    // TODO parse ingredient codes from the payload above
                    this.populateCart(listOf(
                        Ingredient("vinegar", "Apple vinegar"),
                        Ingredient("butter", "Butter"),
                        Ingredient("eggs", "Eggs"),
                        Ingredient("flour", "Flour"),
                        Ingredient("milk", "Milk")
                    ))

                    this.quickReplies("Can I proceed with the purchase?", Pair("✅ Go ahead", "purchase"), Pair("❌ Cancel Order", "cancel"))
                }
            }
        }
        state("purchase") {
            activators {
                regex("✅ Go ahead")
            }

            action {
                val user = reactions.facebook?.queryUserProfile()
                this.sendPayload(
                    TemplateMessage.create(
                        ButtonTemplate.create(
                            "Perfect, ${user?.firstName()}, all done!", listOf(
                                PostbackButton.create("Check order status", "check_order_status")
                            )
                        )
                    )
                )
            }
        }
        state("cancel") {
            activators {
                regex("❌ Cancel Order")
            }

            action {
                val user = reactions.facebook?.queryUserProfile()
                this.sendPayload(TextMessage.create("No worries, ${user?.firstName()}. I've cancelled your order, your credit card was not charged."))
            }
        }
    }
}

private fun ActionContext.populateCart(ingredients: List<Ingredient>): Unit {
    val prices = listOf(10f, 15f, 5f, 3f, 7f, 6.50f, 8.2f, 2.3f)
    val user = reactions.facebook?.queryUserProfile()

    val recipientName = "${user?.firstName()} ${user?.lastName()}"
    val orderNumber = UUID.randomUUID().toString()
    val paymentMethod = "Credit card ending with 4413"
    val currency = "EUR"

    val orderItems = ingredients.map { OrderItem(it.code, it.name, prices.random()) }
    val total = orderItems.map { it.price }.sum()

    val elements = orderItems.map {
        Item.create(it.name, it.price, Optional.empty(), Optional.of(1), Optional.of(currency), Optional.of(URL("https://1cfc270b2aad.ngrok.io/assets/${it.code}.png")))
    }

    val template = ReceiptTemplate.create(
        recipientName,
        orderNumber,
        paymentMethod,
        currency,
        Summary.create(total),
        Optional.empty<Address>(),
        Optional.of(elements),
        Optional.empty<List<Adjustment>>(),
        Optional.of("Tesco"),
        Optional.empty<URL>(),
        Optional.empty<Boolean>(),
        Optional.of(Instant.now())
    )
    this.sendPayload(TemplateMessage.create(template))
}

data class OrderItem(val code: String, val name: String, val price: Float)