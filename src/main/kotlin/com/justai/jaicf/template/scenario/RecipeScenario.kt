package com.justai.jaicf.template.scenario

import com.github.messenger4j.send.message.TemplateMessage
import com.github.messenger4j.send.message.template.GenericTemplate
import com.github.messenger4j.send.message.template.button.PostbackButton
import com.github.messenger4j.send.message.template.common.Element
import com.justai.jaicf.context.ActionContext
import com.justai.jaicf.model.scenario.Scenario
import com.justai.jaicf.template.service.Recipe
import com.justai.jaicf.template.service.RecipeServiceDummyImpl
import java.net.URL
import java.util.*

object RecipeScenario : Scenario() {
    private val recipeService = RecipeServiceDummyImpl()

    init {
        state("hearty_breakfast") {
            activators {
                regex("❤️ Hearty meal")
            }
            action {
                this.carousel(recipeService.heartyBreakfast())
            }
        }
        state("english_breakfast") {
            activators {
                regex("\uD83C\uDF73 English style")
            }
            action {
                this.carousel(recipeService.englishBreakfast())
            }
        }
        state("fruity_breakfast") {
            activators {
                regex("\uD83C\uDF4B Vitamin bomb")
            }
            action {
                this.carousel(recipeService.fruityBreakfast())
            }
        }
        state("meaty_lunch") {
            activators {
                regex("\uD83E\uDD69 Meaty pleasure")
            }
            action {
                this.carousel(recipeService.meatyLunch())
            }
        }
        state("veggie_lunch") {
            activators {
                regex("\uD83E\uDD6C Veggie indulgence")
            }
            action {
                this.carousel(recipeService.vegetarianLunch())
            }
        }
        state("spicy_lunch") {
            activators {
                regex("\uD83E\uDDBE Feeling brave")
            }
            action {
                this.carousel(recipeService.mealOfDayLunch())
            }
        }
        state("light_dinner") {
            activators {
                regex("\uD83D\uDE34 Sweet dreams menu")
            }
            action {
                this.carousel(recipeService.sweetDreamsDinner())
            }
        }
        state("proper_dinner") {
            activators {
                regex("\uD83D\uDE29 Long day sorrows")
            }
            action {
                this.carousel(recipeService.toughDayDinner())
            }
        }
        state("simple_dinner") {
            activators {
                regex("\uD83D\uDE44 Just eat!")
            }
            action {
                this.carousel(recipeService.balancedDinner())
            }
        }
    }
}

private fun ActionContext.carousel(recipes: List<Recipe>): Unit {
    val elements = recipes.map {
        Element.create(
            it.title,
            Optional.of("yummy"),
            Optional.of(URL("https://1cfc270b2aad.ngrok.io/assets/${it.image}.jpg")),
            Optional.empty(),
            Optional.of(
                listOf(
                    PostbackButton.create("View Details", "detail_${it.title}"),
                    PostbackButton.create("Shop Ingredients", "shop_${it.title}_${it.ingredients.map { i -> i.name }.joinToString()}")
                )
            )
        )
    }
    val template = GenericTemplate.create(elements)
    this.sendPayload(TemplateMessage.create(template))
}