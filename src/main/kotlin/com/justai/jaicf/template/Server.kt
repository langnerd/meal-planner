package com.justai.jaicf.template

import com.justai.jaicf.channel.facebook.FacebookChannel
import com.justai.jaicf.channel.facebook.FacebookPageConfig
import com.justai.jaicf.channel.http.httpBotRouting
import io.ktor.application.*
import io.ktor.response.*
import io.ktor.routing.*
import io.ktor.server.engine.embeddedServer
import io.ktor.server.netty.Netty
import java.io.File

fun main() {
    val channel = FacebookChannel(
        templateBot,
        FacebookPageConfig(
            pageAccessToken = System.getenv("FB_PAGE_ACCESS_TOKEN"),
            appSecret = System.getenv("FB_APP_SECRET"),
            verifyToken = System.getenv("FB_WEBHOOK_VERIFY_TOKEN")
        )
    )

    embeddedServer(Netty, 8000) {
        routing {

            httpBotRouting("/" to channel)

            get("/") {
                call.respondText(
                    channel.verifyToken(
                        call.parameters["hub.mode"],
                        call.parameters["hub.verify_token"],
                        call.parameters["hub.challenge"]
                    )
                )
            }

            get("/assets/{filename}") {
                val filename = call.parameters["filename"]
                call.respondFile(File("src/main/resources/assets/$filename"))
            }
        }
    }.start(wait = true)
}