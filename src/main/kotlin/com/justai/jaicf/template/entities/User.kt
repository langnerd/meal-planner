package com.justai.jaicf.template.entities

import org.ktorm.entity.Entity

interface User: Entity<User> {
    val id : Int
    val username : String
}