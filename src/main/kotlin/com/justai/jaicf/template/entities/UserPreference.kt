package com.justai.jaicf.template.entities

import org.ktorm.entity.Entity

interface UserPreference: Entity<UserPreference> {
    val id: Int
    val user: User
    val product: String
    val preferenceType: Int // 1 - positive, 0 - neutral, -1 - negative
    val preferenceReason: String
}