package com.justai.jaicf.template.entities

import org.ktorm.entity.Entity

interface Recipe: Entity<Recipe> {
    val id : Int
    val recipeName : String
    val ingredients : String
    val instructions : String
    val allergyGroups: String
    val tags : String
    val meal: String
    val category: String
    val theme: String
}