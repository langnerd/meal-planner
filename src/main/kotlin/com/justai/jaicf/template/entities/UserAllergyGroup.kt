package com.justai.jaicf.template.entities

import org.ktorm.entity.Entity

interface UserAllergyGroup : Entity<UserAllergyGroup> {
    val user : User
    val allergy : AllergyGroup
}