package com.justai.jaicf.template.entities

import org.ktorm.entity.Entity

interface AllergyGroup: Entity<AllergyGroup> {
    val allergyName: String
    val product: String
}