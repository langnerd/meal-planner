package com.justai.jaicf.template.service

import com.justai.jaicf.template.database.queries.RecipeDatabaseImport
import org.ktorm.database.Database

interface RecipeService {

    fun findAll(): List<Recipe>

    fun findByCriteria(criteria : Criteria): List<Recipe>
    fun heartyBreakfast(): List<Recipe>

    fun findByDefaultCriteria(): List<Recipe>
    fun englishBreakfast(): List<Recipe>

    fun saveAllergies(allergies : List<String>)
    fun fruityBreakfast(): List<Recipe>

    fun savePreferences(preferences : Map<String, Int>)

    fun heartyBreakfast(): List<Recipe>

    fun englishBreakfast(): List<Recipe>

    fun fruityBreakfast(): List<Recipe>

    fun meatyLunch(): List<Recipe>

    fun vegetarianLunch(): List<Recipe>

    fun mealOfDayLunch(): List<Recipe>

    fun sweetDreamsDinner(): List<Recipe>

    fun toughDayDinner(): List<Recipe>

    fun balancedDinner(): List<Recipe>
}

object RecipeServiceFactory {
    fun make(): RecipeService {
        val conn = getConnection()
        val importer = importRecipes(conn)
        return RecipeServiceImpl(conn, importer)
    }

    private fun getConnection(): Database {
        val connString =
            System.getenv("JAWSDB_MARIA_URL") ?: "jdbc:mysql://localhost:3306/mealplanner?user=root&password=password"
        return Database.connect(connString)
    }

    private fun importRecipes(conn: Database): RecipeDatabaseImport {
        return RecipeDatabaseImport(conn)
        // return RecipeDatabaseImport(conn).readFromExcelFile("C:\\Users\\Irina\\Downloads\\recipes.xlsx")
    }
}