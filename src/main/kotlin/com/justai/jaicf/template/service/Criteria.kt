package com.justai.jaicf.template.service

import com.justai.jaicf.template.database.AllergyGroups
import com.justai.jaicf.template.database.UserAllergyGroups
import com.justai.jaicf.template.database.UserPreferences
import com.justai.jaicf.template.entities.AllergyGroup
import org.ktorm.database.Database
import org.ktorm.dsl.*

class Criteria(val conn: Database,
               val mealTypes : List<String>,
               val useAllergies : Boolean,
               val usePreferences : Boolean) {
    val user : Int = 1
    val neutral : Int = 0
    val allergies : List<String> = getUserAllergies()
    var dislikedProducts : List<String> = dislikedProducts()

    private fun getUserAllergies() : List<String> {
        return conn.from(UserAllergyGroups).select(UserAllergyGroups.allergyName)
                .where {
                    UserAllergyGroups.userId eq user
                }
                .map { row -> row.toString() }
    }

    private fun dislikedProducts() : List<String> {
        return conn.from(UserPreferences).select(UserPreferences.product)
                .where {
                    (UserPreferences.userId eq user) and
                    (UserPreferences.preferenceType less neutral)
                }
                .map { row -> row.toString() }
    }

    private fun likedProducts() : List<String> {
        return conn.from(UserPreferences).select(UserPreferences.product)
                .where {
                    (UserPreferences.userId eq user) and
                    (UserPreferences.preferenceType greater neutral)
                }
                .map { row -> row.toString() }
    }
}