package com.justai.jaicf.template.service

import com.justai.jaicf.template.database.Recipes
import com.justai.jaicf.template.database.UserAllergyGroups
import com.justai.jaicf.template.database.UserPreferences
import com.justai.jaicf.template.database.queries.RecipeDatabaseImport
import org.ktorm.database.Database
import org.ktorm.dsl.*
import org.ktorm.entity.sequenceOf
import org.ktorm.entity.toList
import org.ktorm.schema.ColumnDeclaring
import com.justai.jaicf.template.entities.Recipe as RecipeEntity

class RecipeServiceImpl(val db: Database, val importer: RecipeDatabaseImport): RecipeService {

    private val defaultCriteria : Criteria = Criteria(db, emptyList(), useAllergies = true, usePreferences = true)

    override fun findAll(): List<Recipe> {
        return db.sequenceOf(Recipes)
                .toList()
                .map {
                    result -> result.toClient()
                }
    }

    override fun heartyBreakfast(): List<Recipe> {
        TODO("Not yet implemented")
    }

    override fun englishBreakfast(): List<Recipe> {
        TODO("Not yet implemented")
    }

    override fun fruityBreakfast(): List<Recipe> {
        TODO("Not yet implemented")
    }

    override fun meatyLunch(): List<Recipe> {
        TODO("Not yet implemented")
    }

    override fun vegetarianLunch(): List<Recipe> {
        TODO("Not yet implemented")
    }

    override fun mealOfDayLunch(): List<Recipe> {
        TODO("Not yet implemented")
    }

    override fun sweetDreamsDinner(): List<Recipe> {
        TODO("Not yet implemented")
    }

    override fun toughDayDinner(): List<Recipe> {
        TODO("Not yet implemented")
    }

    override fun balancedDinner(): List<Recipe> {
        TODO("Not yet implemented")
    }

    override fun findByCriteria(criteria: Criteria): List<Recipe> {
        return db.from(Recipes).select()
                .where {
                    val conditions = ArrayList<ColumnDeclaring<Boolean>>()
                    if (criteria.useAllergies) {
                        for (allergy in criteria.allergies) {
                            conditions += Recipes.allergyGroups notLike allergy
                        }
                    }
                    if (criteria.usePreferences) {
                        for (product in criteria.dislikedProducts) {
                            conditions += Recipes.ingredients notLike product
                        }
                    }
                    conditions.reduce { a, b -> a and b }
                }
                .map{ row -> Recipes.createEntity(row).toClient() }
    }

    override fun findByDefaultCriteria() : List<Recipe> {
        return findByCriteria(defaultCriteria)
    }

    override fun saveAllergies(allergies : List<String>) {
        val user = 1 // Need to determine user somehow
        for (allergy in allergies) {
            var found = db.from(UserAllergyGroups).select()
                    .where {
                        (UserAllergyGroups.userId eq user) and
                        (UserAllergyGroups.allergyName eq allergy)}
                    .totalRecords == 0
            if (!found) {
                db.insert(UserAllergyGroups) {
                    set(it.userId, user)
                    set(it.allergyName, allergy)
                }
            }
        }

    }

    override fun savePreferences(preferences: Map<String, Int>) {
        val user = 1 // Need to determine user somehow

        for ((prod, pref) in preferences ) {
            var found = db.from(UserPreferences).select()
                    .where {
                        (UserPreferences.userId eq user) and
                        (UserPreferences.product eq prod)
                        (UserPreferences.preferenceType eq pref)
                    }
                    .totalRecords == 0
            if (!found) {
                db.insert(UserPreferences) {
                    set(it.userId, user)
                    set(it.product, prod)
                    set(it.preferenceType, pref)
                }
            }
        }
    }
}

private fun RecipeEntity.toClient(): Recipe =
    Recipe(this.recipeName, "", this.ingredients.split(",").map { Ingredient("", it) })