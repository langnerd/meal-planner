package com.justai.jaicf.template.service

data class Recipe(val title: String, val image: String, val ingredients: List<Ingredient>)

data class Ingredient(val code: String, val name: String)