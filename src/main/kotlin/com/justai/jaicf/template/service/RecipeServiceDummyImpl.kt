package com.justai.jaicf.template.service

class RecipeServiceDummyImpl: RecipeService {
    companion object {
        object Breakfast {
            val english = listOf(
                Recipe("Fried eggs", "english_breakfast_01", emptyList()),
                Recipe("French toast", "english_breakfast_02", emptyList()),
                Recipe("French toast - epic style", "english_breakfast_03", emptyList())
            )
            val fruity = listOf(
                Recipe("Yoghurt & cereals", "fruity_breakfast_01", emptyList()),
                Recipe("Large fruit bowl", "fruity_breakfast_02", emptyList()),
                Recipe("Oatmeal with berries", "fruity_breakfast_03", emptyList())
            )
            val hearty = listOf(
                Recipe("Caesar salad", "hearty_breakfast_01", emptyList()),
                Recipe("Banana pancakes", "hearty_breakfast_02", emptyList()),
                Recipe("Poached egg with toast", "hearty_breakfast_03", emptyList())
            )
        }
        object Lunch {
            val meaty = listOf(
                Recipe("Shredded pork with rice noodles", "meaty_lunch_01", emptyList()),
                Recipe("Pork ribs & French fries", "meaty_lunch_02", emptyList()),
                Recipe("Dirty burger!", "meaty_lunch_03", emptyList()),
                Recipe("Fish fillet", "meaty_lunch_04", emptyList())
            )
            val veggie = listOf(
                Recipe("Yummy dumplings with soya sauce", "vegie_lunch_01", emptyList()),
                Recipe("Quinoa salad", "vegie_lunch_02", emptyList()),
                Recipe("Chickpeas & avocado awesomeness", "vegie_lunch_03", emptyList())
            )
            val spicy = listOf(
                Recipe("Hot stuff surprise!", "spicy_lunch_01", emptyList()),
                Recipe("Rice cake chilly burger", "spicy_lunch_02", emptyList()),
                Recipe("Mexican moussaka", "spicy_lunch_03", emptyList())
            )
        }
        object Dinner {
            val light = listOf(
                Recipe("Mushroom risotto", "light_dinner_01", emptyList()),
                Recipe("Baked eggplant with garlic and onion", "light_dinner_02", emptyList()),
                Recipe("Pad Thai with shrimps", "light_dinner_03", emptyList())
            )
            val proper = listOf(
                Recipe("Sirloin steak with mashed potatoes", "proper_dinner_01", emptyList()),
                Recipe("Fish fillet", "proper_dinner_02", emptyList()),
                Recipe("Chicken Tikka Massala", "proper_dinner_03", emptyList())
            )
            val simple = listOf(
                Recipe("Meat balls with cheese", "simple_dinner_01", emptyList()),
                Recipe("Spaghetti Napolitana", "simple_dinner_02", emptyList()),
                Recipe("Pork ribs & bacon", "simple_dinner_03", emptyList())
            )
        }
    }

    override fun findAll(): List<Recipe> {
        TODO("Not yet implemented")
    }

    override fun findByCriteria(criteria: Criteria): List<Recipe> {
        TODO("Not yet implemented")
    }

    override fun findByDefaultCriteria(): List<Recipe> {
        TODO("Not yet implemented")
    }

    override fun saveAllergies(allergies: List<String>) {
        TODO("Not yet implemented")
    }

    override fun savePreferences(preferences: Map<String, Int>) {
        TODO("Not yet implemented")
    }

    override fun heartyBreakfast(): List<Recipe> =
        Breakfast.hearty

    override fun englishBreakfast(): List<Recipe> =
        Breakfast.english

    override fun fruityBreakfast(): List<Recipe> =
        Breakfast.fruity

    override fun meatyLunch(): List<Recipe> =
        Lunch.meaty

    override fun vegetarianLunch(): List<Recipe> =
        Lunch.veggie

    override fun mealOfDayLunch(): List<Recipe> =
        Lunch.spicy

    override fun sweetDreamsDinner(): List<Recipe> =
        Dinner.light

    override fun toughDayDinner(): List<Recipe> =
        Dinner.proper

    override fun balancedDinner(): List<Recipe> =
        Dinner.simple
}